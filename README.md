REST API application using Golang and PostgreSQL databases.

/v1/products/ - allows you to get list of products
/v1/products/:{id} - allows you to get detailed information of specific product
