package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

var db *sql.DB

const (
	host     = "127.0.0.1"
	port     = 5432
	user     = "api"
	password = "123456"
	dbname   = "api"
	sslmode  = "disable"
)

type Product struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Price       int    `json:"price"`
	Description string `json:"description"`
}

type JsonResponse struct {
	Type    string    `json:"type"`
	Data    []Product `json:"data"`
	Message string    `json:"message"`
}

func main() {

	var err error

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	db, err = sql.Open("postgres", psqlInfo)

	checkErr(err)

	defer db.Close()

	router := mux.NewRouter()

	// Get all products
	router.HandleFunc("/v1/products/", GetProducts).Methods("GET")

	// Get a specific product by the ID
	router.HandleFunc("/v1/products/:{id}", GetProduct).Methods("GET")

	log.Fatal(http.ListenAndServe(":8000", router))
}

// Get all products
func GetProducts(w http.ResponseWriter, r *http.Request) {

	if r.Method != "GET" {
		http.Error(w, "Method is not allowed", 405)
	} else {

		printMessage("Getting products...")

		// Get all products from products table
		rows, err := db.Query("SELECT * FROM products")

		checkErr(err)
		var products []Product
		// var response []JsonResponse
		// Foreach product
		for rows.Next() {
			var id int
			var name string
			var price int
			var description string

			err = rows.Scan(&id, &name, &price, &description)

			checkErr(err)

			products = append(products, Product{ID: id, Name: name, Price: price, Description: description})
		}

		var response = JsonResponse{Type: "success", Data: products}

		json.NewEncoder(w).Encode(response)

	}
}

// Get Product by ID
func GetProduct(w http.ResponseWriter, r *http.Request) {

	if r.Method != "GET" {
		http.Error(w, "Method is not allowed", 405)
	} else {

		params := mux.Vars(r)

		productID := params["id"]

		var response = JsonResponse{}

		printMessage("Print product from DB")

		rows, err := db.Query("SELECT * FROM products where id = $1", productID)
		checkErr(err)

		var products []Product
		// var response []JsonResponse
		// Foreach product
		for rows.Next() {
			var id int
			var name string
			var price int
			var description string

			err = rows.Scan(&id, &name, &price, &description)

			checkErr(err)

			products = append(products, Product{ID: id, Name: name, Price: price, Description: description})
		}

		response = JsonResponse{Type: "success", Data: products}

		json.NewEncoder(w).Encode(response)

	}
}

// Function for handling messages
func printMessage(message string) {
	fmt.Println("")
	fmt.Println(message)
	fmt.Println("")
}

// Function for handling errors
func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
